# BITS Pilani Hyderabad Campus GitLab Group

## Introduction
This group is for showcasing and encouraging the collaboration of technical projects of BITS Pilani, Hyderabad Campus. This group aims to be a helpful resource for those who are interested in programming and for those who want to contribute to the numerous projects started in this campus.

This GitLab group is a read-only mirror of the selected git repositories. The mirror is automatically updated on an hourly basis.

Do *not* try pushing to this mirror! It won't work. Instead, all your pushes to your original git repository will be synced here. Clone the repository from the mirror URL if you want to push to it (provided you have git access to it).

## Project Submission Guidelines
Anything goes as far as project material and choice of programming language is concerned. The essential requirement is that the code **must** be well-documented with good READMEs and/or comments.

As a project maintainer, remember that your code is meant to make it as easy as possible for others to understand your project so that they can contribute to it. Formatting is taken pretty seriously as well - so your code must be well formatted with an easy to read coding style.

Here are some guidelines which can help you see if you prepare your project for this group:

### What to include in your project
* Good documentation with helpful README files and comments. Bonus points for including installation guidelines.
* Good formatting with a consistent coding style throughout all the project files.
* Clear project structure giving a clear overview of the project.
* Incomplete projects are fine as long as they are actively developed and well maintained.
* Student made tutorials are encouraged. For example, see this tutorial for [creating KDE ioslaves](https://github.com/shortstheory/kioslave-tutorial).
* Implementation of competitive programming algorithms and solutions to linked questions are also good submissions.

### What to avoid in your project
* Poor documentation.
* Inconsistent coding style and project structure.
* Your project should not be a re-iteration of very common a application. This is a fairly broad rule and is up to the moderator's discretion, but as a rule of thumb, projects which are the technical equivalent of a "Hello World" are unlikely to make it to this group. Of course, projects written for making a tutorial are an exception to this rule.
* Projects created solely from a tutorial lesson or workshop.
* Incomplete projects which have been abandoned by the project maintainer.
* Forks of open source projects won't be accepted.

## How to Submit Your Project
Fill out the [Google form](https://goo.gl/forms/W0qD6CUGA2huQoRs2) for submitting your project. Alternatively, you contact either Arnav Dhamija or Srimanta Barua on Facebook and message any of us the link to your project. We will review your work and will let you know if your project has been selected in a few days time.

## How to Contribute to a Project
Follow the "Mirrored from" link under the project title. From there, find the maintainer of the project and get in touch with him/her to add you as a collaborator for the project. Your contributions from there will be automatically mirrored back to the GitLab repository.

## How do I create a git repository and upload to GitHub?
Follow [this guide](https://guides.github.com/activities/hello-world/).
